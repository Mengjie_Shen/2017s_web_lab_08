/**
 * Created by mshe666 on 6/12/2017.
 */


function printDatetime() {
    var displayZone = document.getElementById("displayZone");

    displayZone.innerHTML = Date();
}


var intervalTimer = setInterval(printDatetime, 1000);

window.onload = function () {

    var pauseButton = document.getElementById("pauseButton");

    pauseButton.onclick = function () {
        if (intervalTimer != null) {
            clearInterval(intervalTimer);
            intervalTimer = null;
        } else {
            alert("The clock is not running!!");
        }
    }

    var resumeButton = document.getElementById("resumeButton");

    resumeButton.onclick = function () {
        if (intervalTimer == null) {
            intervalTimer = setInterval(printDatetime, 1000);
        } else {
            alert("The clock is already running!!")
        }
    }

}