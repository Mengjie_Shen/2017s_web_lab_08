var customers = [
    {"name": "Peter Jackson", "gender": "male", "year_born": 1961, "joined": "1997", "num_hires": 17000},

    {"name": "Jane Campion", "gender": "female", "year_born": 1954, "joined": "1980", "num_hires": 30000},

    {"name": "Roger Donaldson", "gender": "male", "year_born": 1945, "joined": "1980", "num_hires": 12000},

    {"name": "Temuera Morrison", "gender": "male", "year_born": 1960, "joined": "1995", "num_hires": 15500},

    {"name": "Russell Crowe", "gender": "male", "year_born": 1964, "joined": "1990", "num_hires": 10000},

    {"name": "Lucy Lawless", "gender": "female", "year_born": 1968, "joined": "1995", "num_hires": 5000},

    {"name": "Michael Hurst", "gender": "male", "year_born": 1957, "joined": "2000", "num_hires": 15000},

    {"name": "Andrew Niccol", "gender": "male", "year_born": 1964, "joined": "1997", "num_hires": 3500},

    {"name": "Kiri Te Kanawa", "gender": "female", "year_born": 1944, "joined": "1997", "num_hires": 500},

    {"name": "Lorde", "gender": "female", "year_born": 1996, "joined": "2010", "num_hires": 1000},

    {"name": "Scribe", "gender": "male", "year_born": 1979, "joined": "2000", "num_hires": 5000},

    {"name": "Kimbra", "gender": "female", "year_born": 1990, "joined": "2005", "num_hires": 7000},

    {"name": "Neil Finn", "gender": "male", "year_born": 1958, "joined": "1985", "num_hires": 6000},

    {"name": "Anika Moa", "gender": "female", "year_born": 1980, "joined": "2000", "num_hires": 700},

    {"name": "Bic Runga", "gender": "female", "year_born": 1976, "joined": "1995", "num_hires": 5000},

    {"name": "Ernest Rutherford", "gender": "male", "year_born": 1871, "joined": "1930", "num_hires": 4200},

    {"name": "Kate Sheppard", "gender": "female", "year_born": 1847, "joined": "1930", "num_hires": 1000},

    {"name": "Apirana Turupa Ngata", "gender": "male", "year_born": 1874, "joined": "1920", "num_hires": 3500},

    {"name": "Edmund Hillary", "gender": "male", "year_born": 1919, "joined": "1955", "num_hires": 10000},

    {"name": "Katherine Mansfield", "gender": "female", "year_born": 1888, "joined": "1920", "num_hires": 2000},

    {"name": "Margaret Mahy", "gender": "female", "year_born": 1936, "joined": "1985", "num_hires": 5000},

    {"name": "John Key", "gender": "male", "year_born": 1961, "joined": "1990", "num_hires": 20000},

    {"name": "Sonny Bill Williams", "gender": "male", "year_born": 1985, "joined": "1995", "num_hires": 15000},

    {"name": "Dan Carter", "gender": "male", "year_born": 1982, "joined": "1990", "num_hires": 20000},

    {"name": "Bernice Mene", "gender": "female", "year_born": 1975, "joined": "1990", "num_hires": 30000}
];


function createTable() {
    var div = document.getElementById("tableContainer")

    var table = document.createElement("table");
    table.id = "table";
    table.classList.add("table");
    table.classList.add("table-striped");

    var thead = document.createElement("thead");
    table.appendChild(thead);

    var row = document.createElement("tr");
    thead.appendChild(row);

    var cell1 = document.createElement("th");
    cell1.innerHTML = "Name";
    var cell2 = document.createElement("th");
    cell2.innerHTML = "Gender";
    var cell3 = document.createElement("th");
    cell3.innerHTML = "Year Born";
    var cell4 = document.createElement("th");
    cell4.innerHTML = "Joined";
    var cell5 = document.createElement("th");
    cell5.innerHTML = "Num Hires";
    row.appendChild(cell1);
    row.appendChild(cell2);
    row.appendChild(cell3);
    row.appendChild(cell4);
    row.appendChild(cell5);

    var tbody = document.createElement("tbody");
    tbody.id = "tableBody";
    table.appendChild(tbody);
    div.appendChild(table);

}

function addRow() {
    var tbody = document.getElementById("tableBody");

    for (var index = 0; index < customers.length; index++) {
        var customer = customers[index];
        var name = customer.name;
        var gender = customer.gender;
        var yearBorn = customer.year_born;
        var joind = customer.joined;
        var numHires = customer.num_hires;
        tbody.innerHTML += "<tr><td>" + name + "</td><td>" + gender + "</td><td>" + yearBorn + "</td><td>" + joind + "</td><td>" + numHires + "</td></tr>";
    }

}

function sum() {
    var sumContainer = document.getElementById("sumContainer");

    var maleSum = 0;
    var femaleSum = 0;
    var age0_30 = 0;
    var age31_64 = 0;
    var age65 = 0;
    var gold = 0;
    var silver = 0;
    var bronze = 0;

    for (var index = 0; index < customers.length; index++) {
        var customer = customers[index];
        if (customer.gender == 'male') {
            maleSum++;
        } else {
            femaleSum++;
        }

        if (customer.year_born >= 2007 - 30) {
            age0_30++;
        } else if (customer.year_born >= 2007 - 65) {
            age31_64++;
        } else {
            age65++;
        }

        if (customer.num_hires / ( 52 * (2007 - customer.joined)) >= 4) {
            gold++;
        } else if (customer.num_hires / (52 * (2007 - customer.joined)) >= 1) {
            silver++;
        } else {
            bronze++;
        }
    }


    var genderSum = document.createElement("p");
    genderSum.id = "genderSum";
    genderSum.innerHTML += "Male: " + maleSum + ", Female: " + femaleSum;
    sumContainer.appendChild(genderSum);


    var ageSum = document.createElement("p");
    ageSum.id = "ageSum";
    ageSum.innerHTML += "Age(0-30): " + age0_30 + ", Age(31-64): " + age31_64 + ", Age(65+): " + age65;
    sumContainer.appendChild(ageSum);


    var loyaltySum = document.createElement("p");
    loyaltySum.id = "loyaltySum";
    loyaltySum.innerHTML += "Gold: " + gold + ", Silver: " + silver + ", Bronze: " + bronze;
    sumContainer.appendChild(loyaltySum);



}
